// import thư viện mongose 
const mongoose = require("mongoose");
//improt PrizeHistoryModel 
const prizeHistoryModel = require("../model/prizeHistoryModel");

//Tạo function createVoucherHistory
const createPrizeHistory = (req,res)=>{
    //B1: thu thập dữ liệu từ req
    let body = req.body
    //B2: validate dữ liệu (vì dữ liệu typ là mogoose.Types.ObjectId)
    if(!mongoose.Types.ObjectId.isValid(body.user)){
        return res.status(400).json({
            message: "Id user không hợp lệ"
        })
    };
    if(!mongoose.Types.ObjectId.isValid(body.prize)){
        return res.status(400).json({
            message: "Id prize không hợp lệ"
        })
    };
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    // thu thập dữ liệu
    let newPrizeHistory = {
        _id: mongoose.Types.ObjectId(),
        user: body.user,
        prize: body.prize
    }
    prizeHistoryModel.create(newPrizeHistory,(err,data)=>{
        if(err){
            return res.status(500).json({
                message: err.message
            })
        }
        else{
            return res.status(201).json({
                message:"Tạo mới thành công",
                User: data
            })
        }
    })
};
// Tạo function getAllPrizeHistory
const getAllPrizeHistory = (req,res)=>{
    //B1: thu thập dữ liệu từ req
    // let userCheck = req.query.userCheck;
    // let conditionCheck = {};
    // if(userCheck){
    //     conditionCheck.user = userCheck
    // }
    // có thể tìm theo điều kiện thêm điều kiện tìm theo điều kiện theo hướng dẫn find(condition).function().function().exc..
    //B2: validate dữ liệu
    //B3: Gọi model thực hiện các thao tác nghiệp vụ

    // prizeHistoryModel.find(conditionCheck).exec((err,data)=>{
    //     if(err){
    //         return res.status(500).json({
    //              message: err.message
    //          })
    //      }
    //      else{
    //          return res.status(201).json({
    //              message:"Tải toàn bộ dữ liệu thành công thông qua query param",
    //              User: data
    //          })
    //      }
    // })
    //B1: thu thập dữ liệu từ req
    let user = req.query.user
    let condition = {}
    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(user)) {
        return res.status(400).json({
            message: 'user không hợp lệ'
        })
    }
    if (user) {
        condition.user = user
    }

    prizeHistoryModel.find(condition).populate("user").populate("prize").exec((err,data)=>{ 
        if(err){
            return res.status(500).json({
                 message: err.message
             })
         }
         else{
             return res.status(201).json({
                 message:"Tải dữ liệu thành công",
                 User: data
             })
         }
    })
};
// Tạo function getPrizeHistoryById
const getPrizeHistoryById = (req,res)=>{
    //B1: thu thập dữ liệu từ req
    let id = req.params.historyId
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
          message: 'Id không hợp lệ'
        })
      }
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    prizeHistoryModel.findById(id).populate("user").populate("prize").exec((err,data)=>{
        if(err){
            return res.status(500).json({
                 message: err.message
             })
         }
         else{
             return res.status(201).json({
                 message:"Tải dữ liệu thành công thông qua Id",
                 User: data
             })
         }
    })
};
// Tạo function updatePrizeHistoryById
const updatePrizeHistoryById = (req,res)=>{
    //B1: thu thập dữ liệu từ req
    let id = req.params.historyId;
    let body = req.body;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
          message: 'Id không hợp lệ'
        })
    };
    if(!mongoose.Types.ObjectId.isValid(body.user)){
        return res.status(400).json({
            message: "Id user không hợp lệ"
        })
    };
    if(!mongoose.Types.ObjectId.isValid(body.prize)){
        return res.status(400).json({
            message: "Id prize không hợp lệ"
        })
    };
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    let prizeHistoryUpdate = {
        user: body.user,
        prize: body.prize
      }
    prizeHistoryModel.findByIdAndUpdate(id,prizeHistoryUpdate,(err,data)=>{
        if(err){
            return res.status(500).json({
                 message: err.message
             })
         }
         else{
             return res.status(200).json({
                 message:"Cập nhật dữ liệu thành công bằng  ID",
                 updateData: data
             })
         }
      })
};
// Tạo function deletePrizeHistoryById
const deletePrizeHistoryById = (req,res)=>{
    //B1: thu thập dữ liệu từ req
    let id = req.params.historyId;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
          message: 'Id không hợp lệ'
        })
    };
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    prizeHistoryModel.findByIdAndDelete(id,(err,data)=>{
        if(err){
            return res.status(500).json({
                 message: err.message
             })
         }
         if (data == null) return res.status(404).json({
            notFound: "ID không tìm thấy!"
          })    
         else{
             return res.status(200).json({
                 message:"Thành công xoá dữ liệu thông qua ID",
                 DeleteData: data
             })
         }
    })
}
//exprot controller
module.exports = {
    createPrizeHistory,
    getAllPrizeHistory,
    getPrizeHistoryById,
    updatePrizeHistoryById,
    deletePrizeHistoryById
}

