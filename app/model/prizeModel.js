// khai báo thư viện mongoose
const mongoose = require("mongoose");
// khai báo class schema của mongo
const schema = mongoose.Schema;
//khai báo  review schema
const PrizeShema = new schema ({
    name:{type: String, unique: true, require: true},
    description:{type: String, require: false}
},{timestamps:true})

// exprot mongoose
module.exports = mongoose.model("Prize",PrizeShema);
